adapter containers provide limited set of functionalities to underlying storage object.
but we can use the features of inheritance to access the underlying object and hence more functionalities.
here the example demostrates how you can get the capacity(the actual memory allocated currently) of the underlying object. std::priority_queue does not provide any to get the capacity.


N.B - make sure you don't mess up with the underlying storage object without updating the std::priority_queue object.

to compile - g++ -std=c++11 main.cc

to run - ./a.out


if you want to measure the RAM usage and the benefit, comment one code segment at a time(please refer the code). compile and run. and while the program is running execute the following command. observe the  VSZ and RSS values of the process which gives you an indication of the memory used by the process.

ps aux|grep a.out
