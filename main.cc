#include <iostream>
#include <queue>
#include <stdio.h>

using namespace std;


/*
adaptor containers will have a limited set of functionalities of the underlying container by default.
here in this example we use std::priority_queue and std::vector as the undelying container.
these containers allocate memory dynamically. i.e when the data set gets bigger it allocate more and more memory.
hence the memory reserved gets bigger.
but when elements are removed, memory is not shrinked back automatically. This can become a problem when you deal with systems where
the data set can have high peak values.
the vector has a feature to shrink the memory back. but that feature is not visible to the std::priority_queue.
Here we use sort of a back door to access the  shrink_to_fit() function of the std::vector from std::priority_queue

Note - it is not advisable to access all the functions of the underlying container in this way. which may lead to undefined behaviour.
		also there are some performance costs when you use shrink_to_fit().

*/


template<typename T> void print_queue(T q)
{
	cout << "elements of the queue are :" << endl;
	while (!q.empty())
	{
		cout << q.top() << ",";
		q.pop();
	}
	cout << "\n";
}


template<typename T> 
class HackedPQ : public priority_queue<T>
{
	// HackedPQ():priority_queue<T>()
public:
	int getSize(){return this->c.size();}
	int getCapacity(){return this->c.capacity();}

	void shrink_to_fit(){this->c.shrink_to_fit();}

};


int main ()
{
	
	//code segment one
	//std::priority queue
	cout << "using the std::priority queue..." << endl;
	std::priority_queue<int> original_queue;
	for (int i=0; i<10000000; i++)
		original_queue.push(i);

	//print_queue(original_queue);

	cout << "size :" << original_queue.size() << endl;
	cout << "capacity : can not print the capacity since no method is available to get capacity" << endl;

	cout << "\nremoving first 9999980 elements" << endl;
	for (int i=0; i<9999980; i++)
		original_queue.pop();

	cout << "size :" << original_queue.size() << endl;
	cout << "capacity : can not print the capacity since no method is available to get capacity" << endl;

	cout << "\nnow adding one more element." << endl;
	original_queue.push(10000000);

	cout << "size :" << original_queue.size() << endl;
	cout << "capacity : can not print the capacity since no method is available to get capacity" << endl;


	//code segment 2
	//Hacked one
	cout << "\n\nusing the hacked priority queue" << endl;
	HackedPQ<int> HPQ;

	for (int i=0; i<10000000; i++)
		HPQ.push(i);

	//print_queue(HPQ);

	cout << "size :" << HPQ.getSize() << endl;
	cout << "capacity :" << HPQ.getCapacity() << endl;


	cout << "\nremoving first 9999980 elements" << endl;
	for (int i=0; i<9999980; i++)
		HPQ.pop();

	cout << "size :" << HPQ.getSize() << endl;
	cout << "capacity :" << HPQ.getCapacity() << endl;

	cout << "\nshrinking..." << endl;

	HPQ.shrink_to_fit();

	cout << "size :" << HPQ.getSize() << endl;
	cout << "capacity :" << HPQ.getCapacity() << endl;
	//print_queue(HPQ);	

	cout << "\nnow adding one more element." << endl;
	HPQ.push(10000000);

	cout << "size :" << HPQ.getSize() << endl;
	cout << "capacity :" << HPQ.getCapacity() << endl;
	//print_queue(HPQ);

	getchar();
}
